Steps for the Monitoring Setup
------------------------------

> Create 3 EC2 instances. Use Amazon Linux for convenience.

> Create a new IAM role with following permissions. **“AmazonEC2RoleforSSM, CloudWatchAgentServerPolicy”**

> Select each server > Actions > Security > Modify IAM Role and assign the created IAM Role. Do this for all 3 servers.

> Install cloudwatch agent **“sudo yum install amazon-cloudwatch-agent”**

> Install collectd **“sudo yum install collectd”**

> Goto **“/opt/aws/amazon-cloudwatch-agent/bin”** and enter **“sudo ./amazon-cloudwatch-agent-config-wizard”** this will guide you to enable monitoring for multiple metrics.

> Set the configuration as below when asked,

    - On which OS are you planning to use the agent? - Linux
    - Are you using EC2 or On-Premises hosts? - EC2
    - Which user are you planning to run the agent? - root
    - Do you want to turn on StatsD daemon? -yes
    - Which port do you want StatsD daemon to listen to? - default (8125)
    - What is the collect interval for StatsD daemon? - 60s
    - What is the aggregation interval for metrics collected by StatsD daemon? - 60s
    - Do you want to monitor metrics from CollectD? - yes
    - Do you want to monitor any host metrics? - yes
    - Do you want to monitor cpu metrics per core? - yes
    - Do you want to add ec2 dimensions into all of your metrics if the….? - yes
    - Do you want to aggregate ec2 dimensions (InstanceId)? - yes
    - Would you like to collect your metrics at high resolution? - 60s
    - Which default metrics config do you want? - Standard
    - Are you satisfied with the above config? - yes
    - Do you have any existing CloudWatch Log Agent - no
    - Do you want to monitor any log files? - yes
    - Log file path - /var/log/messages
    - Log group name - “enter something that you can identify the logs”
    - Log group class - STANDARD
    - Log stream name - “Enter something you can distinguish”
    - Log group retention in days - 1
    - Do you want to specify any additional log files to monitor? - no
    - Do you want the CloudWatch agent to also retrieve X-ray traces? - no
    - Do you want to store the config in the SSM parameter store? - no

> Once done, **config.json** will appear in the CWAgent bin directory.

> While staying in the same directory type **“sudo amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:/opt/aws/amazon-cloudwatch-agent/bin/config.json”**

> Check if the service is running by **“sudo systemctl status amazon-cloudwatch-agent.service”**

> Install stress programme to give a boost to metrics for visualization **“sudo yum install stress -y”**

> Now the cloudwatch agent can fetch all the information for Cloud Watch

> Goto cloudwatch console in AWS and create a new dashboard, turn on autosave.

> Now select the metrics you want and add them into the dashboard.

> Note: I got a problem, when considering 5 metrics, can’t allocate a percentage for every metric. Example: network traffic and disk I/O.

> Gauge is good for percentage wise monitoring while line is good for numerical monitoring.

> Goto Amazon SNS service, create a new standard SNS topic, set the endpoint as email and enter the email you want and create the subscription.

> Then confirm the subscription using your email inbox.

> To set email alerts, goto Cloudwatch alarms and add the metric you want to consider and set the threshold, click next > notification  > SNS topic  > select the one you created, click next > preview > create alarm

> To retrieve the logs to cloudwatch > logs > log groups and you will find the logs there.
